from tabulate import tabulate

from db.base import NewSession
from db.models.student import StudentORM


class Solution:
    def __init__(self):
        result = []
        headers = StudentORM.__table__.c
        with NewSession() as db:
            students = db.query(StudentORM)
        for student in students:
            flag = False
            for subject in student.gradebook:
                if subject.estimation == 2:
                    flag = True
            if flag:
                result.append(
                    ["", student.id, student.name, student.surname, student.birth_date]
                )
        print("Result:")
        print(tabulate(result, headers=headers))
