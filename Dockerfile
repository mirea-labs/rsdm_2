FROM python:3.10

WORKDIR /rsdm_2

COPY requirements.txt .

RUN pip install --upgrade pip && pip install -r requirements.txt --no-cache-dir

COPY . .

CMD sh start.sh